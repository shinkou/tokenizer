package com.gitlab.shinkou.tokenizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Starter
{
	static Pattern reToken = Pattern.compile("\\\"[^\"]*\\\"|[^\\\"\\s]+");
	static String QUOTES_AND_SPACES_ON_ENDS = "^[\\\"\\s]+|[\\\"\\s]+$";
	static String BLANK = "";

	public static void addIfNonEmpty(List<String> l, String s)
	{
//		System.out.println("DEBUG: [" + s + "]");
		String sanitized = s.replaceAll(QUOTES_AND_SPACES_ON_ENDS, BLANK);
		if (! sanitized.isEmpty()) l.add(sanitized);
	}

	public static List<String> tokenize(String s)
	{
		List<String> tokens = new ArrayList<>();
		Matcher m = reToken.matcher(s);
		int idx1 = 0, idx2 = 0;
		while(m.find(idx2))
		{
			idx1 = m.start();
			addIfNonEmpty(tokens, s.substring(idx2, idx1));
			addIfNonEmpty(tokens, m.group());
			idx2 = m.end();
		}
		return tokens;
	}

	public static void main(String[] args)
	{
		Stream.of(args).forEach
		(arg -> {
			System.out.println("INPUT:");
			System.out.println(arg);
			System.out.println("============================================================================");
			System.out.println("TOKENS:");
			long timer = System.nanoTime();
			tokenize(arg).stream().forEach(s -> System.out.println(s));
//			System.out.println("ELAPSED: " + (System.nanoTime() - timer) + " nano seconds");
			System.out.println("----------------------------------------------------------------------------");
		});
	}
}
